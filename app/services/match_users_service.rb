class MatchUsersService
  def initialize(fullname)
    @fullname = fullname
    @is_a_match = []
  end

  def perform
    users = User.where('fullname ILIKE ?', "#{@fullname}%")
    @is_a_match = users.select { |user| accept_or_reject_match(user) }
  end

  private

  def accept_or_reject_match(user)
    return if @fullname.length == 2 && user.fullname.length > 2

    if user.fullname.include?(@fullname.capitalize) ||
        (@fullname.length > 3 && user.fullname == @fullname.capitalize)
      @is_a_match.push(user)
    end
  end
end
