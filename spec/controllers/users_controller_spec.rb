require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  describe 'GET #index' do
    it 'returns http success' do
      get :index, format: :json
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #search' do
    let!(:user) { FactoryBot.create(:user, fullname: 'Joe') }
    context 'searching without surrounding whitespaces' do
      it 'returns http success' do
        get :search, params: { user: user.attributes }, format: :json
        expect(response).to have_http_status(:success)
      end
      it 'returns correct user' do
        get :search, params: { user: user.attributes }, format: :json
        user_response = JSON.parse(response.body, symoblize_names: true).first
        expect(user_response).to eq(user.fullname)
      end
    end
    context 'searching with surrounding whitespaces' do
      it 'returns correct user' do
        get :search, params: { user: { fullname: '  Joe  ' } }, format: :json
        user_response = JSON.parse(response.body, symoblize_names: true).first
        expect(user_response).to eq(user.fullname)
      end
    end
  end
end
