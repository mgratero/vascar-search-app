# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

# Ruby version
- 2.6.6

# System dependencies
- Node, Postgresql

# Configuration
- clone the project
- run bundle install

# Database creation
- run rails db:create
- run rails db:migrate

# Database initialization
- run rails db:seed

# How to run the test suite
-run rspec
